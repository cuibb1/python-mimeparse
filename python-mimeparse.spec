Name:           python-mimeparse
Version:        1.6.0
Release:        8
Summary:        Python module for handling mime-types.
License:        MIT
URL:            https://github.com/dbtsai/python-mimeparse
Source0:        https://files.pythonhosted.org/packages/source/p/python-mimeparse/%{name}-%{version}.tar.gz
BuildArch:      noarch

%description
This module provides basic functions for handling mime-types.
It can handle matching mime-types against a list of media-ranges.

%package        -n python2-mimeparse
Summary:        Python2 module for handing mime-type
BuildRequires:  python2-devel python2-setuptools
%{?python_provide:%python_provide python2-mimeparse}

%description    -n python2-mimeparse
This module provides basic functions for handling mime-types.
It can handle matching mime-types against a list of media-ranges.


%package        -n python3-mimeparse
Summary:        Python3 module for handling mime-types.
BuildRequires:  python3-devel python3-setuptools
%{?python_provide:%python_provide python3-mimeparse}

%description    -n python3-mimeparse
This module provides basic functions for handling mime-types.
It can handle matching mime-types against a list of media-ranges.

%prep
%autosetup -n %{name}-%{version}

%build
%py2_build
%py3_build

%install
%py2_install
%py3_install

%check
%{__python2} -m unittest -v mimeparse_test
%{__python3} -m unittest -v mimeparse_test

%files -n python2-mimeparse
%license LICENSE
%doc README.rst
%{python2_sitelib}/*

%files -n python3-mimeparse
%license LICENSE
%doc README.rst
%{python3_sitelib}/*

%changelog
* Fri Nov 29 2019 zhujunhao <zhujunhao5@huawei.com> - 1.6.0-8
- Package init
